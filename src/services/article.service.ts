import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Article } from 'src/entity/article';
import { Observable } from 'rxjs';

const getAllUrl: string = "https://api.spaceflightnewsapi.net/v3/articles"

const getById: string = "https://api.spaceflightnewsapi.net/v3/articles/"

const getCount: string = "https://api.spaceflightnewsapi.net/v3/articles/count"

@Injectable()
export class ArticleService {
    constructor(private http: HttpClient) { }

    public getAllArticles(): Observable<Article[]> {
        return this.http.get<Article[]>(getAllUrl);
    }
    public getArticleById(id: number): Observable<Article> {
        return this.http.get<Article>(getById + id);
    }
    public getCount(): Observable<number> {
        return this.http.get<number>(getCount);
    }
    public getArticlesByPage(pageSize: number, startIndex: number): Observable<Article[]> {
        let map = { params: { _start: startIndex, _limit: pageSize } }
        return this.http.get<Article[]>(getAllUrl, map);
    }
}