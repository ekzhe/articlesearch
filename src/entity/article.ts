import { Launch } from "./launch";
import { Event } from "./event";

export class Article {

    id?: number;
    featured?: boolean;
    title?: string;
    url?: string;
    imageUrl?: string;
    newsSite?: string;
    summary?: string;
    publishedAt?: string;
    latches?: Launch[];
    events?: Event[];

}
