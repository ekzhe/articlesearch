import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from 'src/entity/article';
import { ArticleService } from 'src/services/article.service';

import { MessageService } from 'primeng/api';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';


interface NewsSite {
  name: string,
  code: string
}

@Component({
  selector: 'article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css'],
  providers: [MessageService]
})



export class ArticleFormComponent implements OnInit {


  uploadedFiles: any[] = [];

  article: Article = {};

  addFlag: boolean = true;
  allNewsSites: NewsSite[];

  featuredStr: string = "false";

  articleFormGroup = this.fb.group({
    id: new FormControl(this.article.id, [
      Validators.required,
      Validators.minLength(4)
    ]),
    summary: ['', Validators.required],
    featured: ['', Validators.required],
    newsSite: ['', Validators.required],
    url: ['', Validators.required]
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private articleService: ArticleService,
    private messageService: MessageService,
    private fb: FormBuilder) {

    this.allNewsSites = [
      { name: 'Teslarati', code: 'Teslarati' },
      { name: 'Arstechnica', code: 'Arstechnica' },
      { name: 'SpaceNews', code: 'SpaceNews' },
      { name: 'NASA', code: 'NASA' }
    ];

  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.has("id")) {
        let id = params.get("id");
        this.articleService.getArticleById(Number(id)).subscribe(
          res => {
            this.article = res
            this.addFlag = false;
            this.featuredStr = res.featured + "";
          }
        )
      } else {
        this.addFlag = true
      }
    });
  }

  onUpload(event: { files: any; }) {
    for (let file of event.files) {
      this.uploadedFiles.push(file);
    }
  }

  addSave() {
    // mock to add an article POST api
    this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Add Article Success.' })
    // 延时1.5执行读取方法
    setTimeout(() => {
      this.router.navigate(['article/list/']);
    }, 1500)
  }

  editSave() {
    // mock to update an article PUT api
    this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Update Article Success.' })
    // 延时1.5执行读取方法
    setTimeout(() => {
      this.router.navigate(['article/list/']);
    }, 1500)

  }

  onSubmit() {
    console.warn(this.articleFormGroup.value)
  }

}
function forbiddenNameValidator(arg0: RegExp): import("@angular/forms").ValidatorFn {
  throw new Error('Function not implemented.');
}

