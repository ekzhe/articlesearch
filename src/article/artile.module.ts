import { ModuleWithProviders, NgModule } from '@angular/core';

import { ArticleListComponent } from './list/article-list.component';
import { ArticleDetailComponent } from './detail/article-detail.component';
import { ArticleFormComponent } from './form/article-form.component';

import { HttpClientModule } from '@angular/common/http';


import { ArticleService } from 'src/services/article.service';


import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { FileUploadModule } from 'primeng/fileupload';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { TagModule } from 'primeng/tag';


import { FormsModule } from '@angular/forms';
import { ArticleRoutingModule } from './article-routing.module';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		ButtonModule,
		TableModule,
		CardModule,
		InputTextModule,
		ArticleRoutingModule,
		FormsModule,
		InputTextareaModule,
		RadioButtonModule,
		DropdownModule,
		FileUploadModule,
		ToastModule,
		TagModule,
		ReactiveFormsModule
	],
	declarations: [ArticleListComponent, ArticleDetailComponent, ArticleFormComponent],
	exports: [ArticleListComponent, ArticleDetailComponent, ArticleFormComponent],
})
export class ArticleModule {

	static forRoot(): ModuleWithProviders<ArticleModule> {
		return {
			ngModule: ArticleModule,
			providers: [ArticleService, MessageService]
		}
	}
}
