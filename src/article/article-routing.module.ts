import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticleListComponent } from './list/article-list.component';
import { ArticleDetailComponent } from './detail/article-detail.component';
import { ArticleFormComponent } from './form/article-form.component';

const articleRoutes: Routes = [
  { path: 'article', redirectTo: '/article/list', pathMatch: "full" },
  { path: 'article/list', component: ArticleListComponent },
  { path: 'article/add', component: ArticleFormComponent, pathMatch: 'full' },
  { path: 'article/:id', component: ArticleDetailComponent },
  { path: 'article/edit/:id', component: ArticleFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(articleRoutes)],
  exports: [RouterModule]
})
export class ArticleRoutingModule { }
