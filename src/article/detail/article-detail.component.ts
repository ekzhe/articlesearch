import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { Article } from 'src/entity/article';
import { ArticleService } from 'src/services/article.service';

@Component({
  selector: 'article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {

  article : Article = {}
  loading : boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private articleService: ArticleService) { }

  ngOnInit() {
    this.loading = true;
    this.route.paramMap.subscribe(params => {
      if (params.has("id")) {
        let id = params.get("id");

        this.articleService.getArticleById(Number(id)).subscribe(
          res => {
            this.article = res
            this.loading = false;
          }
        )
      }

    });
  }

}
