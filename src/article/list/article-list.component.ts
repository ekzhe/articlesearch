import { Component, OnInit } from '@angular/core';
import { ArticleService } from 'src/services/article.service';
import { LazyLoadEvent, PrimeNGConfig } from 'primeng/api';
import { Article } from 'src/entity/article';

import { FilterService } from 'primeng/api';


import { Router } from '@angular/router';


@Component({
	selector: 'article-list',
	templateUrl: './article-list.component.html',
	styleUrls: ['./article-list.component.css']
})

export class ArticleListComponent implements OnInit {

	loading: boolean = true;

	count: number = 0;

	constructor(private primengConfig: PrimeNGConfig,
		private articleService: ArticleService,
		private router: Router,
		private filterService: FilterService
	) { }

	ngOnInit() {
		this.primengConfig.ripple = true;
		this.getCount();
		this.loadAllArtiles();
		this.loading = false;
	}

	title = 'articlesearch';

	articleList: Article[] = [];

	articleListCopy: Article[] = [];

	startIndex = 1;

	pageSize = 10;

	showFilter = false;

	showTable = true;

	rowsPerPage: number[] | undefined;

	loadAllArtiles(): void {
		this.articleService.getAllArticles().subscribe(
			res => {
				this.articleList = res
			}
		)
	}

	getCount(): void {
		this.articleService.getCount().subscribe(
			res => {
				this.count = res
			}
		)
	}

	public showDetails(id: number): void {
		this.router.navigate([`article/${id}`]);
	}
	public editArticle(id: number): void {
		this.router.navigate([`article/edit/${id}`]);
	}

	getArticlesByCondition(event: LazyLoadEvent): void {
		this.loading = true;
		this.pageSize = event.rows ? event.rows : this.pageSize;
		this.startIndex = typeof event.first !== 'undefined' ? event.first + 1 : this.startIndex;
		this.articleService.getArticlesByPage(this.pageSize, this.startIndex).subscribe(
			res => {
				this.articleList = res
				this.articleListCopy = [...res]
				this.loading = false;
			}
		)
	}
	doSearch(event: Event): void {
		const keyword = (event.target as HTMLInputElement).value;
		this.articleList = this.articleListCopy.filter(
			value => {
				return value.summary.indexOf(keyword) > -1 || value.title.indexOf(keyword) > -1|| value.newsSite.indexOf(keyword) > -1
			})
		this.count = this.articleList.length
	}
}