import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ArticleModule } from 'src/article/artile.module';
import {ToastModule} from 'primeng/toast';

import { ArticleService } from 'src/services/article.service';
import { MessageService } from 'primeng/api';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ArticleModule,
    BrowserAnimationsModule,
    ToastModule
  ],
  providers: [ArticleService,MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
