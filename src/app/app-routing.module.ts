import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticleListComponent } from 'src/article/list/article-list.component';
import { ArticleDetailComponent } from 'src/article/detail/article-detail.component';

const routes: Routes = [

  { path: '', redirectTo: '/article/list', pathMatch: 'full' },
  { path: 'article', loadChildren:  () => import('./../article/artile.module').then(m => m.ArticleModule) }
]; 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
